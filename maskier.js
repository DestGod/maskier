export function maskier(input, mask, replacements) {

    let charactersRules = [],
        specialCharacters = ['\\', '^', '$', '*', '+', '?', '.', '(', ')', '[', ']', '{', '}'],
        firstNonStatic = null;


    mask.split('').forEach((character,index) => {
        if (typeof replacements[character] === 'undefined') {
            charactersRules.push(checkStaticCharacterForSpecial(character, specialCharacters));
        } else {
            if (firstNonStatic === null)
                firstNonStatic = index;
            charactersRules.push({
                isStatic: false,
                regexp: replacements[character]
            });
        }

    });

    input.addEventListener('input', (event) => {
        input.value = filterData(input.value, charactersRules, firstNonStatic).substring(0, charactersRules.length);
    });

    input.addEventListener('focus', (event) => {
        input.value = replaceStaticCharacters(input.value, charactersRules).substring(0, charactersRules.length);
    });


    function checkStaticCharacterForSpecial(character, characters) {
        return {
            isStatic: true,
            regexp: new RegExp(characters.includes(character) ? `\\${character}` : character),
            character: character
        };
    }

    function filterData(data, charactersRules,firstNonStatic) {

        data = data.split('');
        let inputPosition = data.length,
            cutPosition = null;

        charactersRules.forEach((characterRule, index) => {
            if (characterRule.isStatic && index <= inputPosition && inputPosition >= firstNonStatic) {
                data[index] = characterRule.character;
            }

            if (typeof data[index] !== 'undefined' && !characterRule.regexp.test(data[index]) && !cutPosition) {
                if (index <= inputPosition) {
                    cutPosition = index;
                }
            }
        });

        if (cutPosition !== null) {
            if (cutPosition > 0) {
                data.splice(cutPosition);
            } else {
                data = [];
            }
        }

        return data.join('');
    }

    function replaceStaticCharacters(data, charactersRules) {
        data = data.split('');
        let inputPosition = data.length,
            isStaticsRow = true;

        charactersRules.forEach((characterRule, index) => {
            if ((characterRule.isStatic && isStaticsRow) || (characterRule.isStatic && index <= inputPosition)) {
                data[index] = characterRule.character;
            } else {
                isStaticsRow = false;
            }
        });

        return data.join('');
    }

}
